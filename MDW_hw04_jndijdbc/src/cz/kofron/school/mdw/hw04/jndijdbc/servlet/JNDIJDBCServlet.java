package cz.kofron.school.mdw.hw04.jndijdbc.servlet;

import java.io.IOException;
import java.io.PrintStream;
import java.io.Writer;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import weblogic.jndi.Environment;

/**
 * Added by Filip Kofron on 8.11.14.
 */
public class JNDIJDBCServlet extends HttpServlet
{
	@Override
	protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException
	{
		Writer writer =  httpServletResponse.getWriter();

		Connection connection = getConnection();
		try
		{
			if(connection == null)
			{
				throw new Exception("Fail to retrieve connection.");
			}

			Statement statement = connection.createStatement();
			statement.execute("SELECT * FROM test");
			ResultSet rs = statement.getResultSet();


			writer.write("Results:\n");
			while (rs.next())
			{
				writer.write("test = " + rs.getString(1) + ", foobar = " + rs.getString(2) + "\n");
			}

		} catch (Exception e)
		{
			e.printStackTrace(new PrintStream(System.err));
			writer.write("An error has occurred. Sorry.");
		}
	}

	private Context getContext()
	{
		Environment env = new Environment();
		env.setProviderURL("t3://127.0.0.1:7001");
		try
		{
			Context ctx = env.getInitialContext();

			return ctx;
		} catch (NamingException e)
		{
			e.printStackTrace(new PrintStream(System.err));
		}

		return null;
	}

	private Connection getConnection()
	{
		Context context = getContext();

		try
		{
			DataSource ds
					= (DataSource) context.lookup("hw04_mdw_jdbc");
			return ds.getConnection();
		} catch (NamingException e)
		{
			e.printStackTrace(new PrintStream(System.err));
		} catch (SQLException e)
		{
			e.printStackTrace(new PrintStream(System.err));
		}

		return null;
	}
}
