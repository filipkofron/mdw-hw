package cz.kofron.school.mdw.hw05;

import java.util.HashMap;

import javax.jws.*;

@WebService
public class BankAccount
{
	private static HashMap<Integer, Double> accounts = new HashMap<Integer, Double>();
	
	static
	{
		accounts.put(1, 100.0);
		accounts.put(2, 666.6);
		accounts.put(3, 10.1);
		accounts.put(4, 3.1415926);
	}
	
	@WebMethod
	public boolean validateAccountNumber(int accountNumber)
	{
		return accounts.containsKey(accountNumber);
	}
	
	@WebMethod
	public boolean validateAccountBalance(int accountNumber, double balance)
	{
		return accounts.get(accountNumber) >= balance;
	}
	
	@WebMethod
	public void changeBalance(int accountNumber, double change)
	{
		accounts.put(accountNumber, accounts.get(accountNumber) + change);
	}
}
