package cz.kofron.school.mdw.hw04.rmi;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

import cz.kofron.school.mdw.hw04.rmi.model.CurrencyConverter;
import cz.kofron.school.mdw.hw04.rmi.model.CurrencyConverterImpl;

/**
 * Added by Filip Kofron on 8.11.14.
 */
public class Server
{
	public static void main(String [] args) throws RemoteException, MalformedURLException
	{
		LocateRegistry.createRegistry(1099);

		CurrencyConverter currencyConverter = new CurrencyConverterImpl();
		Naming.rebind("//0.0.0.0/cz.kofron.school.mdw.hw04.rmi.CurrencyConverter", currencyConverter);

		System.out.println("RMI Server has started.");
	}
}
