package cz.kofron.school.mdw.hw04.rmi.model;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Added by Filip Kofron on 8.11.14.
 */
public class CurrencyConverterImpl extends UnicastRemoteObject implements CurrencyConverter
{
	private final static double EUR_USD = 1.25;
	private final static double GBP_USD = 1.59;

	public CurrencyConverterImpl() throws RemoteException
	{
		super();
	}

	public double toUSD(String from, double amount) throws RemoteException
	{
		if(from.equals("USD"))
		{
			return amount;
		}
		if(from.equals("EUR"))
		{
			return amount * EUR_USD;
		}
		if(from.equals("GBP"))
		{
			return amount * GBP_USD;
		}

		throw new RemoteException("Invalid source currency.");
	}

	public double USDTo(String to, double amount) throws RemoteException
	{
		if(to.equals("USD"))
		{
			return amount;
		}
		if(to.equals("EUR"))
		{
			return amount / EUR_USD;
		}
		if(to.equals("GBP"))
		{
			return amount / GBP_USD;
		}

		throw new RemoteException("Invalid destination currency.");
	}

	@Override
	public double convert(String from, String to, double amount) throws RemoteException
	{
		return USDTo(to, toUSD(from, amount));
	}
}
