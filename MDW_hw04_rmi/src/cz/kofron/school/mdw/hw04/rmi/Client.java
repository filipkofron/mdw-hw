package cz.kofron.school.mdw.hw04.rmi;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import cz.kofron.school.mdw.hw04.rmi.model.CurrencyConverter;

/**
 * Added by Filip Kofron on 8.11.14.
 */
public class Client
{
	public static void main(String [] args) throws RemoteException, NotBoundException, MalformedURLException
	{
		Object ob = Naming.lookup("rmi://0.0.0.0/cz.kofron.school.mdw.hw04.rmi.CurrencyConverter");
		CurrencyConverter currencyConverter = (CurrencyConverter) ob;

		System.out.println("10 USD to EUR: " + currencyConverter.convert("USD", "EUR", 10));
		System.out.println("5 GBP to EUR: " + currencyConverter.convert("GBP", "EUR", 5));
		System.out.println("17 EUR to USD: " + currencyConverter.convert("EUR", "USD", 17));
		System.out.println("20 EUR to GBP: " + currencyConverter.convert("EUR", "GBP", 20));
		System.out.println("4 GBP to USD: " + currencyConverter.convert("GBP", "USD", 4));
	}
}
