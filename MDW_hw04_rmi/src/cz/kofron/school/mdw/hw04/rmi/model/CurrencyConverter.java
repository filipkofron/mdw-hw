package cz.kofron.school.mdw.hw04.rmi.model;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Added by Filip Kofron on 8.11.14.
 */
public interface CurrencyConverter extends Remote
{
	public double convert(String from, String to, double amount) throws RemoteException;
}
