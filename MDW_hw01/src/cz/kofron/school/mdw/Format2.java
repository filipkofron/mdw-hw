package cz.kofron.school.mdw;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Represents the structure of Format2 as a tree of nodes.
 *
 * It can store a value or other children of this type. Not both.
 * 
 * @author Filip Kofron
 */
public class Format2
{
    // children with names
    private Map<String, Format2> children = new LinkedHashMap<>();
    
    // the value of this node
    private String value = null;
    
    /**
     * Adds other node only if it has no children.
     * 
     * @param name the children name
     * @param format the children node
     * @throws FormatException when a value is set to this parent node
     */
    public void add(String name, Format2 format) throws FormatException
    {
        if(value != null)
        {
            throw new FormatException("Not a parent node! (value is already set)");
        }
        children.put(name, format);
    }

    /**
     * Construct empty Format2.
     */
    public Format2()
    {
    }

    /**
     * Construct Format2 with value already set.
     * @param value 
     */
    public Format2(String value)
    {
        this.value = value;
    }

    /**
     * Generate number of spaces.
     * 
     * @param level number of spaces
     * @return string of spaces
     */
    private String pre(int level)
    {
        String ret = "";
        for(int i = 0; i < level; i++)
        {
            ret += " ";
        }
        return ret;
    }
    
    /**
     * Recursively print itself to a string.
     * 
     * @param level the depth in the tree
     * @return string of printed itself
     */
    private String printRec(int level)
    {
        String ret = "";
        
        if(value == null)
        {
            ret += "{\n";
            
            Set<Map.Entry<String, Format2>> entrySet = children.entrySet();
            int max = entrySet.size();
            int i = 0;
            for(Map.Entry<String, Format2> entry : entrySet)
            {
                ret += pre(level) + "\"" + entry.getKey() + "\"" + ": ";
                ret += entry.getValue().printRec(level + 2);
                if(i++ < max - 1)
                {
                    ret += ',';
                }
                ret += '\n';
            }
            ret += pre(level - 2) + "}";
        }
        else
        {
            ret += "\"" + value + "\"";
        }
        
        return ret;
    }
    
    @Override
    public String toString()
    {
        return printRec(2);
    }

    /**
     * Set the value, will disallow children.
     * 
     * @param value 
     */
    public void setValue(String value) throws FormatException
    {
        if(children.size() != 0)
        {
            throw new FormatException("The node has children already. (cannot have value as well)");
        }
        this.value = value;
    }
}
