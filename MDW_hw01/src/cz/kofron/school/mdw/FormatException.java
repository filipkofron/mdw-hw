package cz.kofron.school.mdw;

/**
 * Any format exception.
 * 
 * @author Filip Kofron
 */
public class FormatException extends Exception
{
    public FormatException(String message)
    {
        super(message);
    }

    public FormatException(Throwable cause)
    {
        super(cause);
    }
}
