package cz.kofron.school.mdw;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

/**
 * A helper for java's Date formatting according to Format1 and Format2.
 * 
 * @author Filip Kofron
 */
public class DateFormatter
{
    public final static String format1 = "dd MMMM yyyy";
    public final static DateFormat formatter1 = new SimpleDateFormat(format1, Locale.US);
    
    public final static String format2 = "dd.MM.yyyy";
    public final static DateFormat formatter2 = new SimpleDateFormat(format2, Locale.US);
    
    /**
     * Strip all characters that are not numbers.
     * 
     * @param src source string
     * @return string containing only numbers
     */
    private static String stripNonNumbers(String src)    
    {
        String ret = "";
        
        for(int i = 0; i < src.length(); i++)
        {
            char c = src.charAt(i);
            if(c >= '0' && c <= '9')
            {
                ret += c;
            }
        }
        
        return ret;
    }
    
    /**
     * Converts Format1 representation of date to Java's date.
     * 
     * @param format1Date string date
     * @return java Date
     * @throws FormatException when a format error occurs
     */
    public static Date readDateFormat1(String format1Date) throws FormatException
    {
        Scanner scanner = new Scanner(format1Date);
        String token = scanner.next();
        
        String fullStr = stripNonNumbers(token) + " " + scanner.next();
        fullStr += " " + scanner.next();
        
        try
        {
            return formatter1.parse(fullStr);
        }
        catch (ParseException ex)
        {
            throw new FormatException(ex);
        }
    }
    
    /**
     * Converts Java's date to Format2 string representation of it.
     * 
     * @param date instance of Date
     * @return string format of Date
     */
    public static String writeDateFormat2(Date date)
    {
        return formatter2.format(date);
    }
}
