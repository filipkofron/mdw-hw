package cz.kofron.school.mdw;

/**
 * Represents a location divided into 3 attributes.
 * 
 * @author Filip Kofron
 */
class Location
{
    private final String street;
    private final String city;
    private final String zip;

    public Location(String street, String city, String zip)
    {
        this.street = street;
        this.city = city;
        this.zip = zip;
    }
    
    public Location(String format1)
    {
        String [] parts = format1.split(",");
        street = parts[0].trim();
        zip = parts[1].trim();
        city = parts[2].trim();
    }
    
    public String getStreet()
    {
        return street;
    }

    public String getCity()
    {
        return city;
    }
    public String getZip()
    {
        return zip;
    }

    /**
     * Exports itself to Format2 node instance.
     * 
     * @return Format2 instance 
     * @throws FormatException when a Format2 exception occurs.
     */
    public Format2 writeFormat2() throws FormatException
    {
        Format2 where = new Format2();
        
        where.add("street", new Format2(street));
        where.add("city", new Format2(city));
        where.add("zip", new Format2(zip));
        
        return where;
    }
}
