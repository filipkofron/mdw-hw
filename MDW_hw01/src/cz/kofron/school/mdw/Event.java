package cz.kofron.school.mdw;

import java.util.Date;

/**
 * Represents the event.
 * 
 * @author Filip Kofron
 */
public class Event
{
    private String name;
    private Date start;
    private Location location;
    private String summary;
    
    /**
     * Read the attributes from format1.
     * 
     * @param format1 instance of Format1
     * @throws FormatException when a FormatException occurs
     */
    public void fromFormat1(Format1 format1) throws FormatException
    {
        name = format1.getValue("name");
        start = DateFormatter.readDateFormat1(format1.getValue("start"));
        location = new Location(format1.getValue("location"));
        summary = format1.getValue("sumary");
    }
    
    /**
     * Return event converted to Format2.
     * 
     * @return instance of Format2.
     * @throws FormatException when a FormatException occurs
     */
    public Format2 toFormat2() throws FormatException
    {
        Format2 root = new Format2();
        root.add("what", new Format2(name + " - " + summary));
        root.add("when", new Format2(DateFormatter.writeDateFormat2(start)));

        Format2 where = location.writeFormat2();
        root.add("where", where);

        return root;
    }
}
