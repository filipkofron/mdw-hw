package cz.kofron.school.mdw;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;

/**
 * The main class.
 * 
 * @author Filip Kofron
 */
public class MDW_hw01
{
    /**
     * Program startup entry.
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FormatException, FileNotFoundException, Exception
    {
        if(args.length != 2)
        {
            String msg = "Invalid amount of arguments.\n";
            msg += "The correct arguments are: input_file_name output_file_name";
            throw new Exception(msg);
        }
        
        // load format1
        Format1 format1 = new Format1();
        format1.load(new Scanner(new FileInputStream(args[0])));

        Event event = new Event();
        event.fromFormat1(format1);
        
        // convert to format2
        Format2 format2 = event.toFormat2();
        
        // print format2
        PrintStream ps = new PrintStream(args[1]);
        System.out.println(format2.toString());
        ps.println(format2.toString());
    }
}
