package cz.kofron.school.mdw;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Class describes the Format1 structure and allows reading itself
 * from file.
 * 
 * This can be represented as a first level section (with no sub sections).
 * 
 * @author Filip Kofron
 */
public class Format1
{
    // the section name
    private String name = "";
    
    // mapped names to values
    private final Map<String, String> nodes = new HashMap<>();
    
    /**
     * Reads name from format -someName.
     * 
     * @param line line with name
     * @return discovered name or null of empty (may mean the end of section.)
     * @throws FormatException when no - is present.
     */
    private String readName(String line) throws FormatException
    {
        char c = line.charAt(0);
        if(c != '-')
        {
            throw new FormatException("Invalid name.");
        }
        
        String ret = line.substring(1);
        if(ret.isEmpty())
        {
            return null;
        }
        
        return ret;
    }
    
    /**
     * Same as readName, however, expects one more - infront of the name.
     * 
     * @param line line with subName
     * @return the subName
     * @throws FormatException 
     */
    private String readSubName(String line) throws FormatException
    {
        char c = line.charAt(0);
        if(c != '-')
        {
            throw new FormatException("Invalid subname.");
        }
        
        String ret = line.substring(1);
        if(ret.isEmpty())
        {
            return null;
        }
        
        ret = readName(ret);
        
        if(ret == null)
        {
            throw new FormatException("Not a subname.");
        }
        
        if(ret.isEmpty())
        {
            throw new FormatException("Empty subname.");
        }
        
        return ret;
    }
    
    /**
     * Reads the format from passed scanner.
     * 
     * @param scanner Scanner instance with textual data.
     * @throws FormatException when a format problem is found
     */
    public void load(Scanner scanner) throws FormatException
    {
        nodes.clear();
        if(!scanner.hasNextLine())
        {
            throw new FormatException("Empty input.");
        }
        name = readName(scanner.nextLine());

        while(scanner.hasNextLine())
        {
            String subNameLine = scanner.nextLine();
            String subName = readSubName(subNameLine);
            
            if(subName == null)
            {
                // correct end
                return;
            }
            
            String subVal = scanner.nextLine();
            
            nodes.put(subName, subVal);
        }
        
        throw new FormatException("Format doesn't end correctly.");
    }
    
    /**
     * Returns value binded to the given name.
     * 
     * @param name given name
     * @return the binded value
     * @throws FormatException when a name is not found
     */
    public String getValue(String name) throws FormatException
    {
        String ret = nodes.get(name);
        
        if(ret == null)
        {
            throw new FormatException("Value for name '" + name + "' not found.");
        }
        
        return ret;
    }
}
