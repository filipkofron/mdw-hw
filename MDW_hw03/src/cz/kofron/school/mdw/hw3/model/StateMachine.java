package cz.kofron.school.mdw.hw3.model;

import java.util.HashMap;

/**
 * Created by kofee on 1.11.14.
 */
public class StateMachine
{
	private static StateMachine instance = new StateMachine();

	private HashMap<String, User> uidMap = new HashMap<>();

	public static StateMachine getInstance()
	{
		return instance;
	}

	public void moveUserToState(User user) throws Exception
	{
		UserState nextState = UserState.nextState(uidMap.get(user.getId()).getUserState());

		user.setUserState(nextState);

		if(nextState == null)
		{
			throw new Exception("Invalid user session id.");
		}

		if(nextState == UserState.COMPLETED)
		{
			uidMap.remove(user.getId());
		}
	}

	public User addUser()
	{
		User user = User.createUser();
		UserState nextState = UserState.NEW;
		user.setUserState(nextState);
		uidMap.put(user.getId(), user);
		return user;
	}

	public User getUser(String uid)
	{
		return uidMap.get(uid);
	}
}
