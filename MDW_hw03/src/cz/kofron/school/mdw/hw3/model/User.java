package cz.kofron.school.mdw.hw3.model;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Random;

/**
 * Created by kofee on 1.11.14.
 */
public class User
{
	private String id;
	private UserState userState;

	public User(String id)
	{
		this.id = id;
	}

	private static Random rand = new Random();
	public static User createUser()
	{
		MessageDigest digest = null;
		try
		{
			digest = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
			return null;
		}
		byte [] bytes = new byte[64];
		rand.nextBytes(bytes);
		bytes[5] ^= System.currentTimeMillis();
		bytes[11] ^= System.currentTimeMillis();
		bytes[63] ^= System.currentTimeMillis();
		byte[] hash = digest.digest(bytes);
		Base64.Encoder encoder = Base64.getEncoder();
		return new User(encoder.encodeToString(hash));
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		User user = (User) o;

		if (id != null ? !id.equals(user.id) : user.id != null)
		{
			return false;
		}

		return true;
	}

	public String getId()
	{
		return id;
	}

	public UserState getUserState()
	{
		return userState;
	}

	public void setUserState(UserState userState)
	{
		this.userState = userState;
	}

	@Override
	public int hashCode()
	{
		return id != null ? id.hashCode() : 0;
	}
}
