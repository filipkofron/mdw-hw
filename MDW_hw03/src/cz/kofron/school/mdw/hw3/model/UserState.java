package cz.kofron.school.mdw.hw3.model;

/**
 * Created by kofee on 1.11.14.
 */
public enum UserState
{
	NEW, PAYMENT, COMPLETED;

	public static UserState nextState(UserState state)
	{
		switch(state)
		{
			case NEW:
				return PAYMENT;
			case PAYMENT:
				return COMPLETED;
			default:
				return COMPLETED;
		}
	}
}
