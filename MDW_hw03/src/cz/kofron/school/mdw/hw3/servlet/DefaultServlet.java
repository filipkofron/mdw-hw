package cz.kofron.school.mdw.hw3.servlet;

import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.plaf.nimbus.State;

import cz.kofron.school.mdw.hw3.model.StateMachine;
import cz.kofron.school.mdw.hw3.model.User;
import cz.kofron.school.mdw.hw3.model.UserState;

/**
 * Created by Filip Kofron on 27.10.14.
 */
public class DefaultServlet extends HttpServlet
{
	@Override
	protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException
	{
		Cookie [] cookiesArray = httpServletRequest.getCookies();
		Cookie uidCookie = null;
		if(cookiesArray != null)
		{
			for(Cookie cookie : cookiesArray)
			{
				if(cookie.getName().equals("uid"))
				{
					uidCookie = cookie;
				}
			}
		}

		UserState state;
		Writer writer = httpServletResponse.getWriter();

		if(uidCookie == null)
		{
			User user = StateMachine.getInstance().addUser();
			httpServletResponse.addCookie(new Cookie("uid", user.getId()));
			state = user.getUserState();
		}
		else
		{
			String id = uidCookie.getValue();
			try
			{
				User user = StateMachine.getInstance().getUser(id);
				StateMachine.getInstance().moveUserToState(user);
				if(user.getUserState() == UserState.COMPLETED)
				{
					uidCookie.setMaxAge(0);
					httpServletResponse.addCookie(uidCookie);
				}
				state = user.getUserState();
			} catch (Exception e)
			{
				writer.write("INVALID UID");
				writer.write("\n\n");
				return;
			}
		}


		writer.write(state.name());
		writer.write("\n\n");
	}
}
