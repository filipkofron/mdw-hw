package cz.kofron.school.mdw.hw05.composition;

import java.net.MalformedURLException;
import java.net.URL;

import javax.jws.*;
import javax.xml.namespace.QName;

import cz.kofron.school.mdw.hw05.BankAccountService;

@WebService
public class Composition2
{

	@WebMethod
	public boolean transfer(int from, int to, double amount)
	{
		try
		{
			QName name = new QName("http://hw05.mdw.school.kofron.cz/", "BankAccountService");
			BankAccountService bankAccountService = new BankAccountService(
					new URL("http://127.0.0.1:7001/hw05/BankAccountService?WSDL"),
					name);
			if(!bankAccountService.getBankAccountPort().validateAccountNumber(from))
			{
				return false;
			}
			if(!bankAccountService.getBankAccountPort().validateAccountNumber(to))
			{
				return false;
			}
			if(!bankAccountService.getBankAccountPort().validateAccountBalance(from, amount))
			{
				return false;
			}
			bankAccountService.getBankAccountPort().changeBalance(from, -amount);
			bankAccountService.getBankAccountPort().changeBalance(to, amount);
			return true;
		}
		catch (MalformedURLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        return false;
	}
}
